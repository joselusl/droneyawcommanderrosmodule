/*
 * 
 *
 *  Created on: 
 *      Author: Jose Luis Sanchez-Lopez
 */


#include "droneYawCommanderROSModule.h"



using namespace std;




DroneYawCommander::DroneYawCommander() : DroneModule(droneModule::active,FREQ_YAW_PLANNER)
{
    if(!init())
        cout<<"Error init"<<endl;


    return;
}



DroneYawCommander::~DroneYawCommander()
{
	close();
	return;
}

void DroneYawCommander::readParameters()
{
    // Config file
    //
    ros::param::get("~drone_yaw_commander_config_file", configFile);
    if ( configFile.length() == 0)
    {
        configFile="drone_yaw_commander_config.xml";
    }
    std::cout<<"drone_yaw_commander_config_file="<<configFile<<std::endl;

    // Topic names
    //
    ros::param::get("~drone_pose_topic_name", dronePoseTopicName);
    if ( dronePoseTopicName.length() == 0)
    {
        dronePoseTopicName="EstimatedPose_droneGMR_wrt_GFF";
    }
    std::cout<<"drone_pose_topic_name="<<dronePoseTopicName<<std::endl;
    //
    ros::param::get("~drone_point_to_look_topic_name", dronePointToLookTopicName);
    if ( dronePointToLookTopicName.length() == 0)
    {
        dronePointToLookTopicName="dronePointToLook";
    }
    std::cout<<"drone_point_to_look_topic_name="<<dronePointToLookTopicName<<std::endl;
    //
    ros::param::get("~drone_yaw_to_look_topic_name", droneYawToLookTopicName);
    if ( droneYawToLookTopicName.length() == 0)
    {
        droneYawToLookTopicName="droneYawToLook";
    }
    std::cout<<"drone_yaw_to_look_topic_name="<<droneYawToLookTopicName<<std::endl;
    //
    ros::param::get("~drone_yaw_ref_command_topic_name", droneYawRefCommandTopicName);
    if ( droneYawRefCommandTopicName.length() == 0)
    {
        droneYawRefCommandTopicName="droneControllerYawRefCommand";
    }
    std::cout<<"drone_yaw_ref_command_topic_name="<<droneYawRefCommandTopicName<<std::endl;

    return;
}

int DroneYawCommander::readConfigFile(std::string configFile)
{
    //XML document
    pugi::xml_document doc;
    std::ifstream nameFile(configFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        cout<<"I cannot open xml"<<endl;
        return 0;
    }

    std::string readingValue;
    pugi::xml_node drone_yaw_commander = doc.child("drone_yaw_commander");

    // Yaw tolerance
    double yaw_tolerance;
    readingValue=drone_yaw_commander.child_value("yaw_tolerance");
    istringstream convert_yaw_tolerance(readingValue);
    convert_yaw_tolerance>>yaw_tolerance;

    this->yawTolerance=yaw_tolerance*M_PI/180.0;

    std::cout<<"Configurations:="<<std::endl;
    std::cout<<"-yaw_tolerance(rads)="<<yawTolerance<<std::endl;


    return 1;
}


void DroneYawCommander::open(ros::NodeHandle & nIn, std::string moduleName)
{
	//Node
    DroneModule::open(nIn,moduleName);


    // Parameters
    readParameters();


    // Read config files
    std::string configFileComplete=stackPath+"configs/drone"+stringId+"/"+configFile;
    readConfigFile(configFileComplete);

	
    //// Topics ///
    ///subs
    // rostopic pub -1 /drone1/ArucoSlam_EstimatedPose droneMsgsROS/dronePose -- 123 0.0 0.0 1.5 0.0 0.0 0.0 "a" "a" "a"
    dronePoseSubs = n.subscribe(dronePoseTopicName, 1, &DroneYawCommander::dronePoseCallback, this);

    dronePointToLookSub = n.subscribe(dronePointToLookTopicName, 1, &DroneYawCommander::dronePointToLookCallback, this);
    // rostopic pub -1 /drone1/droneYawToLook droneMsgsROS/droneYawRefCommand -- 123 1.3
    droneYawToLookSub = n.subscribe(droneYawToLookTopicName, 1, &DroneYawCommander::droneYawToLookCallback, this);

    ///pub
    droneYawRefCommandPub=n.advertise<droneMsgsROS::droneYawRefCommand>(droneYawRefCommandTopicName, 1, true);


    //Flag of module opened
    droneModuleOpened=true;


	//End
	return;
}

void DroneYawCommander::dronePoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg)
{
    // Flag
    if(!flagDronePose)
        flagDronePose=true;

    // Store the message
    dronePoseMsg.time=msg->time;
    dronePoseMsg.x=msg->x;
    dronePoseMsg.y=msg->y;
    dronePoseMsg.z=msg->z;
    dronePoseMsg.yaw=msg->yaw;
    dronePoseMsg.pitch=msg->pitch;
    dronePoseMsg.roll=msg->roll;

    // Execute
    run();

    return;
}

void DroneYawCommander::dronePointToLookCallback(const droneMsgsROS::dronePositionRefCommand::ConstPtr& msg)
{
    dronePointToLookMsg.x=msg->x;
    dronePointToLookMsg.y=msg->y;
    dronePointToLookMsg.z=msg->z;

    clearFlags();
    flagPointToLook=true;
    flagNewYawCommand=true;

    run();

    return;
}


void DroneYawCommander::droneYawToLookCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr& msg)
{
    droneYawToLookMsg.header=msg->header;
    droneYawToLookMsg.yaw=msg->yaw;

    clearFlags();
    flagYawToLook=true;
    flagNewYawCommand=true;

    run();

    return;
}



int DroneYawCommander::publishYawRefCommand(droneMsgsROS::droneYawRefCommand droneYawRefCommandIn)
{
    if(droneModuleOpened==false)
        return 0;

    // TODO JL: Esto es una guarrada que hay que quitar. Esta aqui porque el controlador no se puede activar directamente en modo trayectoria
    //ros::Duration(0.5).sleep();

    droneYawRefCommandPub.publish(droneYawRefCommandIn);

    return 1;
}




bool DroneYawCommander::init()
{
    dronePoseMsg.time=0.0;
    dronePoseMsg.x=0.0;
    dronePoseMsg.y=0.0;
    dronePoseMsg.z=0.0;
    dronePoseMsg.yaw=0.0;
    dronePoseMsg.pitch=0.0;
    dronePoseMsg.roll=0.0;

    droneYawRefCommandMsg.yaw=0.0;


    flagDronePose=false;


    yawTolerance=0.0;


    clearFlags();

    //end
    return true;
}



void DroneYawCommander::close()
{

    DroneModule::close();
	return;
}



bool DroneYawCommander::resetValues()
{
    droneYawRefCommandMsg.yaw=0.0;

    clearFlags();

    return true;

}



bool DroneYawCommander::startVal()
{


    //End
    return DroneModule::startVal();
}



bool DroneYawCommander::stopVal()
{
    return DroneModule::stopVal();
}



bool DroneYawCommander::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;


    if(flagYawToLook || flagPointToLook)
    {
        // Header
        droneYawRefCommandMsg.header.stamp = ros::Time::now();
        droneYawRefCommandMsg.header.frame_id="DroneYawCommander";
        //Yaw defined by Yaw
        if(flagYawToLook)
        {
            if(!flagNewYawCommand && droneYawRefCommandMsg.yaw==droneYawToLookMsg.yaw && 0)
                return true;
            else
            {
                if(flagNewYawCommand)
                    flagNewYawCommand=false;
                droneYawRefCommandMsg.yaw=droneYawToLookMsg.yaw;
            }
        }

        //Yaw defined by point
        if(flagPointToLook && flagDronePose)
        {
            // atan2: in the interval [-pi,+pi] radians
            double yawRefCommand=atan2( dronePointToLookMsg.y - dronePoseMsg.y,
                                        dronePointToLookMsg.x - dronePoseMsg.x);

            std::cout<<"yawRefCommand="<<yawRefCommand<<"; droneYawRefCommandMsg.yaw="<<droneYawRefCommandMsg.yaw<<"; tol="<<abs(yawRefCommand-droneYawRefCommandMsg.yaw)<<std::endl;

            if(flagNewYawCommand || abs(yawRefCommand-droneYawRefCommandMsg.yaw)>=yawTolerance)
            {
                if(flagNewYawCommand)
                    flagNewYawCommand=false;
                droneYawRefCommandMsg.yaw = yawRefCommand;
            }
            else
                return true;
        }

        //publish
        publishYawRefCommand(droneYawRefCommandMsg);

        // return
        return true;
    }



    return false;
}


int DroneYawCommander::clearFlags()
{
    flagNewYawCommand=false;

    flagYawToLook=false;
    flagPointToLook=false;

    return 1;
}

