/*
*
*
*
*
*/


/// ROS
#include "ros/ros.h"


#include <stdio.h>
#include <iostream>

//trajectory planner
#include "droneYawCommanderROSModule.h"

#include "nodes_definition.h"




using namespace std;



int main(int argc, char **argv)
{

    ros::init(argc, argv, MODULE_NAME_YAW_PLANNER);
    ros::NodeHandle n; //Este nodo admite argumentos!!


    //Init
    cout<<"Starting Drone Yaw Commander..."<<endl;


    //Yaw planner
    DroneYawCommander MyDroneYawCommander;
    MyDroneYawCommander.open(n,MODULE_NAME_YAW_PLANNER);


    // Async
    ros::spin();


    return 0;

}

